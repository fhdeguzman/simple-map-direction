//
//  MapInteractor.swift
//  directions
//
//  Created by Franz Henri de Guzman on 11/18/23.
//

import UIKit
import CoreLocation
import Foundation
import Alamofire
import SwiftyJSON

protocol MapInteractorProtocol {
    var presenter: MapPresenterProtocol? { get set }
    
    func loadDirection(from sourceCoord: CLLocationCoordinate2D, to destinationCoord: CLLocationCoordinate2D, completionHandler: @escaping (_ routes: [JSON]?, _ error: Error?) -> ())
}

class MapInteractor: MapInteractorProtocol {
    
    var presenter: MapPresenterProtocol?
    
    func loadDirection(from sourceCoord: CLLocationCoordinate2D, to destinationCoord: CLLocationCoordinate2D, completionHandler: @escaping (_ routes: [JSON]?, _ error: Error?) -> ()) {
        
        let origin = "\(sourceCoord.latitude),\(sourceCoord.longitude)"
        let destination = "\(destinationCoord.latitude),\(destinationCoord.longitude)"
        let apiKey = K.API.GMSServicesKey
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&key=\(apiKey)&alternatives=true"
        
        AF.request(url).response { response in
            guard let data = response.data else {
                completionHandler(nil, NSError(domain: "YourAppDomain", code: -1, userInfo: ["error": "No data received"]))
                return
            }
            
            do {
                let json = try JSON(data: data)
                let routes = json["routes"].arrayValue
                completionHandler(routes, nil)
            } catch let error {
                completionHandler(nil, error)
                print(error.localizedDescription)
            }
        }
    }
}
