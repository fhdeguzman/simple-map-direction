//
//  MapViewController.swift
//  directions
//
//  Created by Franz Henri de Guzman on 11/18/23.
//

import GoogleMaps
import CoreLocation
import SwiftyJSON
import UIKit

protocol MapViewControllerProtocol {
    var presenter: MapPresenterProtocol? { get set }
    func initialSetup()
    func setUpCurrentLocation(with location: CLLocation)
    func drawDirectionPath(with routes: [JSON]?)
    func setNavigationTitle(with title: String)
}

class MapViewController: UIViewController, GMSMapViewDelegate {
    var presenter: MapPresenterProtocol?
    var selectedLocation: Location?
    
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var preciseLocationZoomLevel: Float = 15.0
    var approximateLocationZoomLevel: Float = 10.0
    var likelyPlaces:[Any] = []
    
    var markers: [GMSMarker] = []
    var polylines: [GMSPolyline] = []
    var routes: [SwiftyJSON.JSON] = []
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var mapContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presentingViewController?.viewWillAppear(true)
    }
    
    @IBAction func doneButonTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
}


// MARK: MapViewControllerProtocol

extension MapViewController: MapViewControllerProtocol {
    
    func initialSetup() {
        guard let selectedLocation = selectedLocation else { return }
        let options = GMSMapViewOptions()
        options.camera = GMSCameraPosition(latitude: (selectedLocation.getCLLocDegreesLat()), longitude: (selectedLocation.getCLLocDegreesLong()), zoom: 12)
        options.frame = self.mapContainerView.bounds
        mapView = GMSMapView(options:options)
        
        let marker = GMSMarker()
        marker.title = selectedLocation.name
        marker.position = CLLocationCoordinate2D(latitude: (selectedLocation.getCLLocDegreesLat()), longitude: (selectedLocation.getCLLocDegreesLong()))
        marker.map = mapView
        
        self.mapContainerView.addSubview(mapView)
        setUpLocationManager()
    }
    
    func setUpLocationManager() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.delegate = self
    }
    
    
    func setUpCurrentLocation(with location: CLLocation) {
        let sourceLoc = location.coordinate
        let destinationLoc = CLLocationCoordinate2D(latitude: (selectedLocation?.getCLLocDegreesLat())!, longitude: (selectedLocation?.getCLLocDegreesLong())!)
        let mapBounds = GMSCoordinateBounds(coordinate: sourceLoc, coordinate: destinationLoc)
        let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(mapBounds, withPadding: 100.0)
        
        mapView.animate(with: cameraWithPadding)
        
        let iconImage = UIImage(named: "current_location")
        let markerIcon = iconImage?.withRenderingMode(.alwaysTemplate)
        
        // Create the current location marker
        let marker = GMSMarker()
        marker.icon = markerIcon
        marker.position = CLLocationCoordinate2D(latitude: sourceLoc.latitude, longitude: sourceLoc.longitude)
        marker.map = mapView
        marker.iconView?.tintColor = .blue
        presenter?.loadDirection(with: sourceLoc, destinationLoc: destinationLoc)
    }
    
    func setNavigationTitle(with title: String) {
        self.navigationBar.topItem?.title = title
    }
    
    func drawDirectionPath(with routes: [SwiftyJSON.JSON]?) {
        guard let routes = routes else { return }

        mapView.clear()
        markers.removeAll()
        polylines.removeAll()
        self.routes = routes

        // Find the fastest route
        var fastestRouteIndex = 0
        var shortestDuration = Double.infinity

        for (index, route) in routes.enumerated() {
            let legs = route["legs"].arrayValue
            if let durationValue = legs.first?["duration"]["value"].double, durationValue < shortestDuration {
                shortestDuration = durationValue
                fastestRouteIndex = index
            }
        }

        for (index, route) in routes.enumerated() {
            let overviewPolyline = route["overview_polyline"].dictionary
            let points = overviewPolyline?["points"]?.string
            let path = GMSPath(fromEncodedPath: points ?? "")
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 5.0

            // Set different colors for each route
            let colors: [UIColor] = [.systemBlue, .systemGreen, .systemOrange]
            let colorIndex = index % colors.count
            polyline.strokeColor = colors[colorIndex]
            polyline.map = mapView

            addMarker(for: path!.coordinate(at: path!.count() - 1), title: "\(selectedLocation?.name ?? "Destination")", snippet: "")

            let legs = route["legs"].arrayValue
            for leg in legs {
                let distance = leg["distance"].dictionary
                let duration = leg["duration"].dictionary

                let distanceText = distance?["text"]?.string ?? "0 km"
                let durationText = duration?["text"]?.string ?? "0 mins"

                // Show details only for the fastest route
                if index == fastestRouteIndex {
                    setNavigationTitle(with: (selectedLocation?.name ?? "Distance") + ": " + distanceText + durationText)
                }

                // Add markers with travel time for each route
                let startLocation = leg["start_location"].dictionary
                let startLat = startLocation?["lat"]?.double ?? 0.0
                let startLng = startLocation?["lng"]?.double ?? 0.0
                // Calculate a percentage along the polyline path (adjust the percentage as needed)
                let percentage = 0.5
                let markerPosition = path?.coordinate(at: UInt(Int(Double(path?.count() ?? 0) * percentage)))

                let marker = GMSMarker()
                marker.position = markerPosition!
                marker.title = "Route \(index + 1)"
                marker.snippet = "Travel Time: \(durationText)"
                marker.map = mapView

                markers.append(marker)

                // Add polyline for each leg
                let polyline = GMSPolyline(path: path)
                polyline.strokeWidth = 5.0
                polyline.strokeColor = colors[colorIndex]
                polyline.map = mapView
                polylines.append(polyline)
            }
        }

        // Adjust camera to fit all polylines and markers on the map
        if let firstRoute = routes.first, let bounds = calculateBounds(for: firstRoute) {
            mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
        }
    }


    // Helper function to add marker at a specific coordinate
    func addMarker(for coordinate: CLLocationCoordinate2D, title: String, snippet: String) {
        let marker = GMSMarker()
        marker.position = coordinate
        marker.title = title
        marker.snippet = snippet
        marker.map = mapView
        markers.append(marker)
    }



    func calculateBounds(for route: SwiftyJSON.JSON) -> GMSCoordinateBounds? {
        guard let legs = route["legs"].arrayValue.first,
              let startLocation = legs["start_location"].dictionary,
              let endLocation = legs["end_location"].dictionary else {
            return nil
        }

        let startLat = startLocation["lat"]?.double ?? 0.0
        let startLng = startLocation["lng"]?.double ?? 0.0
        let endLat = endLocation["lat"]?.double ?? 0.0
        let endLng = endLocation["lng"]?.double ?? 0.0

        var bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2D(latitude: startLat, longitude: startLng),
                                          coordinate: CLLocationCoordinate2D(latitude: endLat, longitude: endLng))

        // Extend bounds to include polyline coordinates
        if let overviewPolyline = route["overview_polyline"].dictionary,
           let points = overviewPolyline["points"]?.string,
           let path = GMSPath(fromEncodedPath: points) {
            for i in 0..<path.count() {
                let coordinate = path.coordinate(at: i)
                bounds = bounds.includingCoordinate(coordinate)
            }
        }

        // Extend bounds to include marker positions
        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }

        return bounds
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        guard let polyline = overlay as? GMSPolyline,
              let index = polylines.firstIndex(of: polyline),
              index < routes.count else {
            return
        }

        // Retrieve the corresponding route details
        let route = routes[index]

        // Display travel details (customize this part as needed)
        let alert = UIAlertController(title: "Route \(index + 1) Details", message: "Distance: \(route["legs"][0]["distance"]["text"].stringValue)\nDuration: \(route["legs"][0]["duration"]["text"].stringValue)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }


}


extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        presenter?.didLoadCurrentLocation(with: location)
        manager.stopUpdatingLocation()
    }
    
    // Handle authorization for the location manager
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        presenter?.locationManager(manager, didChangeAuthorization: status)
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error here: \(error)")
    }
    
}
